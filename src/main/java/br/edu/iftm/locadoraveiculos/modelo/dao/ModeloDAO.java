package br.edu.iftm.locadoraveiculos.modelo.dao;

import br.edu.iftm.locadoraveiculos.modelo.Fabricante;
import br.edu.iftm.locadoraveiculos.modelo.ModeloCarro;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ModeloDAO {

    private final Connection conexao;

    public ModeloDAO(Connection conexao) {
        this.conexao = conexao;
    }
    
      public ModeloCarro buscar(long codigo) throws SQLException {
        ModeloCarro modelo = null;
        String selecao = "SELECT * FROM modelocarro WHERE codigo = ?";
        try (PreparedStatement pstmt = conexao.prepareStatement(selecao)) {
            pstmt.setLong(1, codigo);
            try (ResultSet rs = pstmt.executeQuery()) {
                if (rs.next()) {
                    FabricanteDAO dao = new FabricanteDAO(conexao);
                    modelo = new ModeloCarro();
                    modelo.setCodigo(rs.getLong(1));
                    modelo.setDescricao(rs.getString(2));
                    modelo.setFabricante(dao.buscar(rs.getLong(3)));
                }
            }
        }
        return modelo;
    }

    public List<ModeloCarro> buscarTodos(Fabricante fabricante) throws SQLException {
        ModeloCarro modelo;
        List<ModeloCarro> modelos = new ArrayList<>();
        String selecao = "SELECT * FROM modelocarro WHERE codigo_fabricante = ?";
        try (PreparedStatement pstmt = conexao.prepareStatement(selecao)) {
            pstmt.setLong(1, fabricante.getCodigo());
            try (ResultSet rs = pstmt.executeQuery()) {
                while (rs.next()) {
                    modelo = new ModeloCarro();
                    modelo.setCodigo(rs.getLong(1));
                    modelo.setDescricao(rs.getString(2));
                    modelo.setFabricante(fabricante);
                    modelos.add(modelo);
                }
            }
        }
        return modelos;
    }

  

}
