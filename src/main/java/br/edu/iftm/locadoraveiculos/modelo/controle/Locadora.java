package br.edu.iftm.locadoraveiculos.modelo.controle;

import br.edu.iftm.locadoraveiculos.modelo.Acessorio;
import br.edu.iftm.locadoraveiculos.modelo.Carro;
import br.edu.iftm.locadoraveiculos.modelo.Fabricante;
import br.edu.iftm.locadoraveiculos.modelo.ModeloCarro;
import br.edu.iftm.locadoraveiculos.modelo.dao.AcessorioDAO;
import br.edu.iftm.locadoraveiculos.modelo.dao.CarroDAO;
import br.edu.iftm.locadoraveiculos.modelo.dao.DAOFactory;
import br.edu.iftm.locadoraveiculos.modelo.dao.FabricanteDAO;
import br.edu.iftm.locadoraveiculos.modelo.dao.ModeloDAO;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Locadora extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        String caminho = request.getServletPath();
        if (caminho.equals("/fabricante/selecionar")) {
            try {
                DAOFactory fabrica = new DAOFactory();
                fabrica.abrirConexao();
                FabricanteDAO dao = fabrica.criarFabricanteDAO();
                List<Fabricante> fabricantes = dao.buscarTodos();
                fabrica.fecharConexao();
                request.setAttribute("fabricantes", fabricantes);
                RequestDispatcher rd = request.getRequestDispatcher(
                        "/selecaofabricante.jsp");
                rd.forward(request, response);
            } catch (SQLException ex) {
                DAOFactory.mostrarSQLException(ex);
            }
        } else if (caminho.equals("/modelo/selecionar")) {
            String codigoFabricante = request.getParameter("fabricante");
            try {
                DAOFactory fabrica = new DAOFactory();
                fabrica.abrirConexao();
                FabricanteDAO daof = fabrica.criarFabricanteDAO();
                Fabricante fabricante = daof.buscar(Long.parseLong(codigoFabricante));
                request.getSession().setAttribute("fabricante", fabricante);
                ModeloDAO daom = fabrica.criarModeloDAO();
                List<ModeloCarro> modelos = daom.buscarTodos(fabricante);
                fabrica.fecharConexao();
                request.setAttribute("modelos", modelos);
                RequestDispatcher rd = request.getRequestDispatcher(
                        "/selecaomodelo.jsp");
                rd.forward(request, response);
            } catch (SQLException ex) {
                DAOFactory.mostrarSQLException(ex);
            }
        } else  if (caminho.equals("/carro/novo")) {
            String codigoModelo = request.getParameter("modelo");
            try {
                DAOFactory fabrica = new DAOFactory();
                fabrica.abrirConexao();
                ModeloDAO daom = fabrica.criarModeloDAO();
                ModeloCarro modelo = daom.buscar(Long.parseLong(codigoModelo));
                request.getSession().setAttribute("modelo", modelo);
                AcessorioDAO daoa = fabrica.criarAcessorioDAO();
                List<Acessorio> acessorios = daoa.buscarTodos();
                request.setAttribute("acessorios", acessorios);
                fabrica.fecharConexao();
                RequestDispatcher rd = request.getRequestDispatcher(
                        "/novocarro.jsp");
                rd.forward(request, response);
            } catch (SQLException ex) {
                DAOFactory.mostrarSQLException(ex);
            }
        } else if (caminho.equals("/carro/inserir")) {
            Carro carro = new Carro();
            carro.setPlaca(request.getParameter("placa"));
            carro.setCor(request.getParameter("cor"));
            carro.setChassi(request.getParameter("chassi"));
            carro.setValorDiaria(Double.parseDouble(request.getParameter("valordiaria")));
            carro.setModelo((ModeloCarro) request.getSession().getAttribute("modelo"));

            String[] codigosAcessorios = request.getParameterValues("acessorios");
            List<Acessorio> acessorios = new ArrayList<>();
            Acessorio acessorio;
            try {
                DAOFactory fabrica = new DAOFactory();
                fabrica.abrirConexao();
                AcessorioDAO daoa = fabrica.criarAcessorioDAO();
                for (String codigoAcessorio : codigosAcessorios) {
                    acessorio = daoa.buscar(Long.parseLong(codigoAcessorio));
                    acessorios.add(acessorio);
                }
                carro.setAcessorios(acessorios);
                CarroDAO dao = fabrica.criarCarroDAO();
                dao.gravar(carro);
                fabrica.fecharConexao();
                request.getSession().removeAttribute("fabricante");
                request.getSession().removeAttribute("modelo");
                RequestDispatcher rd = request.getRequestDispatcher(
                        "/mensagem.jsp");
                rd.forward(request, response);
            } catch (SQLException ex) {
                DAOFactory.mostrarSQLException(ex);
            }
            
        }   else if (caminho.equals("/carro/mostrar")) {
            try {
                DAOFactory fabrica = new DAOFactory();
                fabrica.abrirConexao();
                CarroDAO daoc = fabrica.criarCarroDAO();
                List<Carro> carros = daoc.buscarTodos();
                request.setAttribute("carros", carros);               
                fabrica.fecharConexao();
                RequestDispatcher rd = request.getRequestDispatcher(
                        "/carros.jsp");
                rd.forward(request, response);
            } catch (SQLException ex) {
                DAOFactory.mostrarSQLException(ex);
            }
        }   
    }  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
