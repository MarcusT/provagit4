package br.edu.iftm.locadoraveiculos.modelo.dao;

import br.edu.iftm.locadoraveiculos.modelo.Acessorio;
import br.edu.iftm.locadoraveiculos.modelo.Carro;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class AcessorioDAO {

    private final Connection conexao;

    public AcessorioDAO(Connection conexao) {
        this.conexao = conexao;
    }
    
      public Acessorio buscar(long codigo) throws SQLException {
        Acessorio acessorio = null;
        String selecao = "SELECT * FROM acessorio WHERE codigo = ?";
        try (PreparedStatement pstmt = conexao.prepareStatement(selecao)) {
            pstmt.setLong(1, codigo);
            try (ResultSet rs = pstmt.executeQuery()) {
                if (rs.next()) {
                    acessorio = new Acessorio();
                    acessorio.setCodigo(rs.getLong(1));
                    acessorio.setDescricao(rs.getString(2));
                }
            }
        }
        return acessorio;
    }

 public List<Acessorio> buscarTodos() throws SQLException {
        Acessorio acessorio;
        List<Acessorio> acessorios = new ArrayList<>();
        String selecao = "SELECT * FROM acessorio";
        try (Statement stmt = conexao.createStatement()) {
            try (ResultSet rs = stmt.executeQuery(selecao)) {
                while (rs.next()) {
                    acessorio = new Acessorio();
                    acessorio.setCodigo(rs.getLong(1));
                    acessorio.setDescricao(rs.getString(2));
                    acessorios.add(acessorio);
                }
            }
        }
        return acessorios;
    }
    
    public List<Acessorio> buscarTodos(Carro carro) throws SQLException {
        Acessorio acessorio = null;
        List<Acessorio> acessorios = new ArrayList<>();
        String selecao = "SELECT * FROM acessorio INNER JOIN carro_acessorio ON codigo_acessorio = codigo WHERE codigo_carro = ?";
        try (PreparedStatement pstmt = conexao.prepareStatement(selecao)) {
            pstmt.setLong(1, carro.getCodigo());
            try (ResultSet rs = pstmt.executeQuery()) {
                while (rs.next()) {
                    acessorio = new Acessorio();
                    acessorio.setCodigo(rs.getLong(1));
                    acessorio.setDescricao(rs.getString(2));
                    acessorios.add(acessorio);
                }
            }
        }
        return acessorios;
    }

   

}
