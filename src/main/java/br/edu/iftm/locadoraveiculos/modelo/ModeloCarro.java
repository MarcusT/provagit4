package br.edu.iftm.locadoraveiculos.modelo;

public class ModeloCarro {

    private long codigo;
    private String descricao;
    private Fabricante fabricante;

    public long getCodigo() {
        return codigo;
    }

    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Fabricante getFabricante() {
        return fabricante;
    }

    public void setFabricante(Fabricante fabricante) {
        this.fabricante = fabricante;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (codigo ^ (codigo >>> 32));
        result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
        result = prime * result + ((fabricante == null) ? 0 : fabricante.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof ModeloCarro)) {
            return false;
        }
        ModeloCarro other = (ModeloCarro) obj;
        if (codigo != other.codigo) {
            return false;
        }
        if (descricao == null) {
            if (other.descricao != null) {
                return false;
            }
        } else if (!descricao.equals(other.descricao)) {
            return false;
        }
        if (fabricante == null) {
            if (other.fabricante != null) {
                return false;
            }
        } else if (!fabricante.equals(other.fabricante)) {
            return false;
        }
        return true;
    }

}
