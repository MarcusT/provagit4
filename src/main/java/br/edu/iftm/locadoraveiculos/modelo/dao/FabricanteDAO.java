package br.edu.iftm.locadoraveiculos.modelo.dao;

import br.edu.iftm.locadoraveiculos.modelo.Fabricante;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class FabricanteDAO {

    private final Connection conexao;

    public FabricanteDAO(Connection conexao) {
        this.conexao = conexao;
    }
    
    public Fabricante buscar(long codigo) throws SQLException {
        Fabricante fabricante = null;
        String selecao = "SELECT * FROM fabricante WHERE codigo = ?";
        try (PreparedStatement pstmt = conexao.prepareStatement(selecao)) {
            pstmt.setLong(1, codigo);
            try (ResultSet rs = pstmt.executeQuery()) {
                if (rs.next()) {
                    fabricante = new Fabricante();
                    fabricante.setCodigo(rs.getLong(1));
                    fabricante.setNome(rs.getString(2));
                }
            }
        }
        return fabricante;
    }

    public List<Fabricante> buscarTodos() throws SQLException {
        Fabricante fabricante;
        List<Fabricante> fabricantes = new ArrayList<>();
        String selecao = "SELECT * FROM fabricante";
        try (Statement stmt = conexao.createStatement()) {
            try (ResultSet rs = stmt.executeQuery(selecao)) {
                while (rs.next()) {
                    fabricante = new Fabricante();
                    fabricante.setCodigo(rs.getLong(1));
                    fabricante.setNome(rs.getString(2));
                    fabricantes.add(fabricante);
                }
            }
        }
        return fabricantes;
    }

 
  
}
