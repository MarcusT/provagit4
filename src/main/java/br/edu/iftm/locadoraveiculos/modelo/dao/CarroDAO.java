package br.edu.iftm.locadoraveiculos.modelo.dao;

import br.edu.iftm.locadoraveiculos.modelo.Acessorio;
import br.edu.iftm.locadoraveiculos.modelo.Carro;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CarroDAO {

    private final Connection conexao;

    public CarroDAO(Connection conexao) {
        this.conexao = conexao;
    }

    public void gravar(Carro carro) throws SQLException {
        boolean inseriu = false;
        int resultado;
        String insercao = "INSERT INTO carro (chassi, cor, placa, valorDiaria, codigo_modelo) VALUES (?, ?, ?, ?, ?);";
        try (PreparedStatement pstmt = conexao.prepareStatement(insercao, PreparedStatement.RETURN_GENERATED_KEYS)) {
            pstmt.setString(1, carro.getChassi());
            pstmt.setString(2, carro.getCor());
            pstmt.setString(3, carro.getPlaca());
            pstmt.setDouble(4, carro.getValorDiaria());
            pstmt.setLong(5, carro.getModelo().getCodigo());
            resultado = pstmt.executeUpdate();
            if (resultado == 1) {
                ResultSet rs = pstmt.getGeneratedKeys();
                if (rs.next()) {
                    inseriu = true;
                    carro.setCodigo(rs.getLong(1));
                    rs.close();
                }
                System.out.println("\nInserção bem sucedida.");
            } else {
                System.out.println("A inserção não foi feita corretamente.");
            }
        }

        insercao = "INSERT INTO carro_acessorio (codigo_carro, codigo_acessorio) VALUES (?, ?);";
        try (PreparedStatement pstmt = conexao.prepareStatement(insercao)) {
            for (Acessorio acessorio : carro.getAcessorios()) {
                pstmt.setLong(1, carro.getCodigo());
                pstmt.setLong(2, acessorio.getCodigo());
                resultado = pstmt.executeUpdate();
                if (resultado == 1) {
                    System.out.println("\nInserção bem sucedida.");
                } else {
                    System.out.println("A inserção não foi feita corretamente.");
                }
            }
        }
    }
    public List<Carro> buscarTodos() throws SQLException {
        Carro carro;
        List<Carro> carros = new ArrayList<>();
        String selecao = "SELECT * FROM carro";
        try (Statement stmt = conexao.createStatement()) {
            try (ResultSet rs = stmt.executeQuery(selecao)) {
                ModeloDAO daom = new ModeloDAO(conexao);
                AcessorioDAO daoa = new AcessorioDAO(conexao);
                while (rs.next()) {
                    carro = new Carro();
                    carro.setCodigo(rs.getLong(1));
                    carro.setChassi(rs.getString(2));
                    carro.setCor(rs.getString(3));
                    carro.setPlaca(rs.getString(4));
                    carro.setValorDiaria(rs.getDouble(5));
                    carro.setModelo(daom.buscar(rs.getLong(6)));
                    carro.setAcessorios(daoa.buscarTodos(carro));
                    carros.add(carro);
                }
            }
        }
        return carros;
    }


    
    
}
